import axios from 'axios'
import store from '~/store'
import router from '~/router'
import swal from 'sweetalert2'
import i18n from '~/i18n'

//Request interceptor
axios.interceptors.request.use(request => {
    const token = store.getters['auth/token']
    if (token) {
        request.headers.common['Authorization'] = `Token ` + token
        //console.log(`Token ` + token)
    }

    const locale = store.getters['lang/locale']
    if (locale) {
        request.headers.common['Accept-Language'] = locale
    }
    request.baseURL = 'http://10.1.1.36'
    return request
})

axios.interceptors.response.use(response => response, error => {
    const { status } = error.response
    //Ошибка сервера
    if (status >= 500) {
        swal({
            type: 'error',
            title: i18n.t('500 title'),
            text: i18n.t('500 text'),
            reverseButtons: true,
            confirmButtonText: i18n.t('ok'),
            cancelButtonText: i18n.t('cancel')
        })
    }
    //страничка не найдена
    if (status === 404){
        router.push({name: '404'})
    }

    if (status === 401 && store.getters['auth/check']) {
        swal({
            type: 'warning',
            title: i18n.t('token_expired_alert_title'),
            text: i18n.t('token_expired_alert_text'),
            reverseButtons: true,
            confirmButtonText: i18n.t('ok'),
            cancelButtonText: i18n.t('cancel')
        }).then(() => {
            store.commit('auth/LOGOUT')

            router.push({name: 'login'})
        })
    }
    return Promise.reject(error)
})
