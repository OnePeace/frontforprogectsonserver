const Home = () => import('~/views/Home').then(m => m.default || m)
const Login = () => import('~/views/auth/login').then(m => m.default || m)
const Register = () => import('~/views/auth/register').then(m => m.default || m)
const Contacts = () => import('~/views/Contacts').then(m => m.default || m)
const TestingServer = () => import('~/views/TestingServer').then(m => m.default || m)
const TradingBot = () => import('~/views/TradingBot').then(m => m.default || m)
/* const PasswordEmail = () => import('~/pages/auth/password/email').then(m => m.default || m)
const PasswordReset = () => import('~/pages/auth/password/reset').then(m => m.default || m)
const NotFound = () => import('~/pages/errors/404').then(m => m.default || m) */

const About = () => import('~/views/About').then(m => m.default || m)
/* const Settings = () => import('~/pages/settings/index').then(m => m.default || m)
const SettingsProfile = () => import('~/pages/settings/profile').then(m => m.default || m)
const SettingsPassword = () => import('~/pages/settings/password').then(m => m.default || m) */


export default [
  { path: '/', name: 'home', component: Home },
  { path: '/about/', name: 'About', component: About },
  { path: '/contacts/', name: 'Contacts', component: Contacts },
  { path: '/testing_server/', name: 'TestingServer', component: TestingServer },
  { path: '/trading_bot/', name: 'TradingBot', component: TradingBot },
  
  { path: '/login/', name: 'login', component: Login },
  { path: '/register/', name: 'register', component: Register },

  /* { path: '/settings',
    component: Settings,
    children: [
      { path: '', redirect: { name: 'settings.profile' } },
      { path: 'profile', name: 'settings.profile', component: SettingsProfile },
      { path: 'password', name: 'settings.password', component: SettingsPassword }
    ] },

  { path: '*', component: NotFound } */
]
