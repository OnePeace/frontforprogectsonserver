import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
    balance: '123'
}

// getters
export const getters = {
    balance: state => state.balance
}

// mutations
export const mutations = {
    [types.UPDATE_BALANCE](state, data){
        state.balance = data
    }
}

// actions
export const actions = {
    get_balance({commit}){
        axios.get('/api/account/getbalance')
        .then(responce => {
            console.log(responce)
            commit(types.UPDATE_BALANCE, responce.data)
        })
    }
}
