import * as types from '../mutation-types'
// import axios from 'axios'
// state
export const state = {
    connection: null,
    status: false,
    alert: false
}

// getters
export const getters = {
    status: state => state.status,
    alert: state => state.alert
}

// mutations
export const mutations = {
    [types.CONNECT](state, data){
        state.status = true,
        state.alert = true
        state.connection = data
    },
    [types.DISCONNECT](state){
        state.connection = null
        state.status = false,
        state.alert = true
    },
    [types.CLOSE_ALERT](state){
        state.alert = false
    }
}

// actions
export const actions = {
    connect({commit}, conn){
        commit(types.CONNECT, conn)
    },
    disconnect({commit}){
        commit(types.DISCONNECT)
    },
    closeAlert({commit}){
        commit(types.CLOSE_ALERT)
    }
}