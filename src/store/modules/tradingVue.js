import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
    orders: [],
    positionInformation: [],
    tradindStories: [],
    firstData: true,
    dataCharts: {
        chart: {
            data: [
                
            ],
            settings: {},
            grid: {}
        },
        onchart: [{
            name: 'EMA, 26',
            type: 'TestOverlay',
            data: [],
            settings: {
                upper: 70,
                lower: 30,
                backColor: '#9b9ba316',
                bandColor: '#666'
            }
        },
        {
            name: 'EMA, 12',
            type: 'TestOverlay',
            data: [],
            settings: {
                upper: 70,
                lower: 30,
                backColor: '#9b9ba316',
                bandColor: '#666'
            }
        },
        {
            name: "Orders",
            type: "Trades",
            data: [
                [ 1612184400000, 1, 33856.05, '(opt)<Label>' ]
            ],
            settings: {}
        }],
        offchart: [{
            name: "Balance",
            type: "Channel",
            data: [],
            settings: {}
        },
        {
            name: "CCI",
            type: "RSI",
            data: [],
            settings: {
                "upper": 150,
                "lower": -150
            }
        }]
    }
}

// getters
export const getters = {
    dataCarts: state =>state.dataCharts,
    posInf: state => state.positionInformation,
    orders: state => state.orders,
    klinesCount: state => state.dataCharts.chart.data.length,
}

// mutations
export const mutations = {
    [types.ADD_DATA](state, data){
        state.dataCharts.onchart[0].data.push([data.klines[data.klines.length-1].openTime, data.klines[data.klines.length-1].ema_26])
        state.dataCharts.onchart[1].data.push([data.klines[data.klines.length-1].openTime, data.klines[data.klines.length-1].ema_12])
        state.dataCharts.chart.data.push([data.klines[data.klines.length-1].openTime, data.klines[data.klines.length-1].open, data.klines[data.klines.length-1].high, data.klines[data.klines.length-1].low, data.klines[data.klines.length-1].close, data.klines[data.klines.length-1].volume])
        state.dataCharts.offchart[0].data.push([data.klines[data.klines.length-1].openTime, data.balance])
        state.positionInformation = data.posInf
        state.dataCharts.offchart[1].data.push([data.klines[data.klines.length-1].openTime, data.klines[data.klines.length-1].cci])
    },
    [types.ORDER](state, data){
        var dt = JSON.parse(data)
        state.tradindStories.push(dt)
        state.dataCharts.onchart[2].data.push([dt.timestamp, dt.side=="BUY" ? 1 : 0, dt.curPrice, dt.positionSide])
        state.orders.push(dt)
    },
    [types.CLEAR_DATA](state){
        state.dataCharts.chart.data = []
        state.orders = []
        state.dataCharts.onchart.forEach(item => {
            item.data = []
        })
        state.dataCharts.offchart.forEach(item => {
            item.data = []
        })
    },
    [types.ADD_API_DATA](state, payload){
        console.log(payload.data)
        payload.data.klines.forEach(item => {
            state.dataCharts.chart.data.push([item.openTime, item.open, item.high, item.low, item.close, item.volume])
            state.dataCharts.onchart[0].data.push([item.openTime, item.ema_26])
            state.dataCharts.onchart[1].data.push([item.openTime, item.ema_12])

            state.dataCharts.offchart[1].data.push([item.openTime, item.cci])
        })
        payload.data.order_history.forEach(item => {
            state.dataCharts.onchart[2].data.push([item.timestamp, item.side=="BUY" ? 1 : 0, item.curPrice, item.positionSide])
            state.orders.push(item)
        })
        payload.data.balance_history.forEach(item => {
            state.dataCharts.offchart[0].data.push([item.time, item.balance])
        })
        state.positionInformation = payload.data.posInf
    }
}

// actions
export const actions = {
    addData({commit}, payload){
        commit(types.ADD_DATA, payload)
    },
    order({commit}, payload){
        commit(types.ORDER, payload)
    },
    clearData({commit}){
        commit(types.CLEAR_DATA)
    },
    getData({commit}){
        return new Promise((resolve, reject) => {
            axios.get('api/trading/getdata')
            .then(response => {
                commit(types.ADD_API_DATA, response)
                resolve(true)
            })
            .catch(err => {
                reject(err)
            })
        })
        
        
    }
}
