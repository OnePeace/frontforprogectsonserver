import * as types from '../mutation-types'

// state
export const state = {
    navigation: {
        drawer: true,
        drawerRight: true,
        left: false,
        right: false,
        tmpDrawer: ''
    }
}

// getters
export const getters = {
    navigation: state => state.navigation
}

// mutations
export const mutations = {
    [types.CHANGE_LEFT](state){
        state.navigation.drawer = !state.navigation.drawer
    },
    [types.CHANGE_RIGHT](state){
        state.navigation.drawerRight = !state.navigation.drawerRight
    },
    [types.CHANGE_LEFT_TMP](state, payload){
        state.navigation.left = !state.navigation.left
        state.navigation.tmpDrawer = payload
    }
}

// actions
export const actions = {
    leftDrawer({commit}){
        commit(types.CHANGE_LEFT)
    },
    rightDrawer({commit}){
        commit(types.CHANGE_RIGHT)
    },
    leftTMPDrawer({commit}, content){
        commit(types.CHANGE_LEFT_TMP, content)
    }
}
