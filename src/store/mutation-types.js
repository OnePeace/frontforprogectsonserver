// auth.js
export const LOGOUT = 'LOGOUT'
export const SAVE_TOKEN = 'SAVE_TOKEN'
export const FETCH_USER = 'FETCH_USER'
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS'
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE'
export const UPDATE_USER = 'UPDATE_USER'

// lang.js
export const SET_LOCALE = 'SET_LOCALE'

export const UPDATE_BALANCE = 'UPDATE_BALANCE'

export const ADD_DATA = 'ADD_DATA'
export const BALANCE = 'BALANCE'
export const ORDER = 'ORDER'
export const CLEAR_DATA = 'CLEAR_DATA'
export const ADD_API_DATA = 'ADD_API_DATA'

//Wrapers
export const CHANGE_LEFT = 'CHANGE_LEFT'
export const CHANGE_RIGHT = 'CHANGE_RIGHT'
export const CHANGE_LEFT_TMP = 'CHANGE_LEFT_TMP'

//WebSocket
export const CONNECT = 'CONNECT'
export const DISCONNECT = 'DISCONNECT'
export const CLOSE_ALERT = 'CLOSE_ALERT'

//change vuex store for chart
export const STORE = {
    TestingServer: 'tradingVue',
    TradingBot: 'bot'
}

//bot
export const START_KLINES = 'START_KLINES'
