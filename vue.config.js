let path = require('path');
module.exports = {
  transpileDependencies: [
    'vuetify'
  ],

  pluginOptions: {
    i18n: {
      locale: 'ru',
      fallbackLocale: 'ru',
      localeDir: 'locales',
      enableInSFC: true
    },
    webpack: {
      dir: [
        './webpack'
      ]
    }
  },
  configureWebpack: {
    resolve: {
        alias:{
            // 'vue$': 'vue/dist/vue.common.js',
            '~':path.join(__dirname, './src'),
        },
        extensions: ['.js', '.vue', '.json']
    }
  }
}
